# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 15:31:39 2020

@author: 52812
"""
def validate(username):
    """
    :param username: (string) The username to validate.
    :returns: (Boolean) A boolean which determines if the username is valid according to the following rules: 1)Username has a
    minimum length of 4. 2) Username can only contain letters, numbers and optionally one underscore. 3)Username can only start with a letter.
    4)Username can not end with an underscore.
    """
    #Validate username length
    if len(username)<4:
        return False
    else:
        #Validate if the first character of the username is a letter
        if username[0].isalpha()!=True:
                return False
        else:
            #Validate if the username doesn't end in an underscore
            if username[-1]=='_':
                return False
            else:
                underscoreCount=0
                #Validate if the username contains only numbers and letters 
                for i  in range(len(username)):
                    if username[i]!='_' and username[i].isalnum()!=True:
                        return False
                    if username[i]=='_':
                        underscoreCount+=1
                #Validate if the username contains only one underscore
                if underscoreCount>1:
                    return False,
                else:
                    return True 


