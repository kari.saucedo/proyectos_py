# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 15:31:39 2020

@author: 52812
"""
def johnAndMary(a):
    """
    :param a: (string) The string to validate
    :returns: (Boolean) A Boolean that determines if there is the same amount of the word 'John' and the word 'Mary' in the string.
    """
    #Convert the string to lowercase so that it will comply with being case-insensitive
    a=a.lower()
    #Validate if the number of times each word appears is equal
    if a.count('john')==a.count('mary'):
        return True
    return False
