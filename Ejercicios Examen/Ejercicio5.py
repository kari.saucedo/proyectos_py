# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 16:35:39 2020

@author: 52812
"""

def fibonacci(n):
    """
    :param a: (int) The element in the fibonacci sequence
    :returns: (int) The value of the nth element in the fibonacci square
    """  
    #Tried assigning multiple values on a single line on this exercise since we only work with 3 values
    #Initialize loop variable, first element of the sequence and second element of the sequence
    i,previousElement, nextElement = 1,0, 1
    if n>=0 and n<=1:
        return 1
    else:
        #Loop up to n
        while i < n:
            #Reassign the values for the prevoius and next elements
            previousElement, nextElement = nextElement, previousElement+nextElement
            #Increment loop variable by one
            i+=1
            #If we have completed the loop up to n, return the next element
            if i==n:
                return nextElement

