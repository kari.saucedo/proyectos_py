# -*- coding: utf-8 -*-
"""
Created on Wed Feb  5 15:24:04 2020

@author: 52812
"""
import math
def areas(r=10, a=90):
    """
    :param r: (float) The radius of the circle.
    :param a: (float) The angle of the segment.
    :returns: (list) (A list of two elements containing the area inside, and area outside the circle, in that order.) 
    """
    #Calculate circle area
    circleArea=math.pi*math.pow(r,2)
    #Calculate segment area
    segmentArea=(math.pow(r,2)/2)*(a*(math.pi/180)-math.sin(a*(math.pi/180)))
    #Store the results on a list per the problem's description 
    result=[segmentArea,circleArea-segmentArea]
    return result
