# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 22:18:07 2020

@author: 52812
"""

def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    newWord = ''
    for letter in secretWord:
        if letter in lettersGuessed:
            newWord += letter
        else:
            newWord += ' _ '

    return newWord
secretWord = 'apple' 
lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']
print(getGuessedWord(secretWord, lettersGuessed))