# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 22:18:59 2020

@author: 52812
"""
import string
def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    letterList = string.ascii_lowercase
    newList = ''
    for letter in letterList:
        if letter not in lettersGuessed:
            newList += letter
        else:
            newList += ''

    return newList
lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']
print(getAvailableLetters(lettersGuessed))
