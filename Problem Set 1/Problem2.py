# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 17:18:12 2020

@author: 52812
"""

"""
Assume s is a string of lower case characters.

Write a program that prints the number of times the string 'bob' occurs in s. 
For example, if s = 'azcbobobegghakl', then your program should print.
"""
s='azcbobobegghakl'
counter=0
lower=0
upper=len('bob')
for i in s:
    if 'bob'==s[lower:upper]:
        counter+=1
    upper+=1
    lower+=1
print(counter)