# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 17:28:44 2020

@author: 52812
"""

"""
Same description as Problem 2 but with the added requirement of using bisection search to make the program faster
"""
balance = 89543; 
annualInterestRate = 0.18
monIntRate = (annualInterestRate / 12.0)
paymentLower = (balance / 12)
paymentUpper = (balance * ((1 + monIntRate)**12)) / 12.0
origBalance = balance
while balance != 0.00:
    thePayment = (paymentLower + paymentUpper) / 2
    balance = origBalance
    for i in range(1,13):
        balance = ((balance - thePayment) * (1 + monIntRate))
    if balance > 0:
        paymentLower = thePayment
    elif balance < 0:  
        paymentUpper = thePayment
    balance = round(balance, 2)
print(round(thePayment,2))
