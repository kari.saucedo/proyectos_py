# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 17:23:24 2020

@author: 52812
"""
"""
Write a program to calculate the credit card balance after one year if a person only pays the minimum monthly payment
required by the credit card company each month.

The following variables contain values as described below:

    balance - the outstanding balance on the credit card

    annualInterestRate - annual interest rate as a decimal

    monthlyPaymentRate - minimum monthly payment rate as a decimal

For each month, calculate statements on the monthly payment and remaining balance. 
At the end of 12 months, print out the remaining balance. 
Be sure to print out no more than two decimal digits of accuracy - so print
Remaining balance: 813.41
"""
# Paste your code into this box
from decimal import Decimal
balance=484
annualInterestRate=0.2
monthlyPaymentRate=0.04
def creditCardBalance(balance,annualInterestRate,monthlyPaymentRate):    
    monthlyInterestRate = annualInterestRate/12.0
    totalPay = 0    
    for i in range(1,13):       
        minMonPay = balance * monthlyPaymentRate 
        totalPay += minMonPay
        balance = balance - minMonPay 
        balance = balance * (1 + monthlyInterestRate)
    return round(balance,2)   
print("Remaining balance:",creditCardBalance(balance,annualInterestRate,monthlyPaymentRate))