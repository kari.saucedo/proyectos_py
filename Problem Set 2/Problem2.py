# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 17:26:17 2020

@author: 52812
"""
"""
Now write a program that calculates the minimum fixed monthly payment needed in 
order pay off a credit card balance within 12 months. By a fixed monthly payment, we mean a single number 
which does not change each month, but instead is a constant amount that will be paid each month.

In this problem, we will not be dealing with a minimum monthly payment rate.

The following variables contain values as described below:

    balance - the outstanding balance on the credit card

    annualInterestRate - annual interest rate as a decimal

The program should print out one line: the lowest monthly payment that will pay off all debt in under 1 year, for example:

Lowest Payment: 180 

Assume that the interest is compounded monthly according to the balance at the end of the month 
(after the payment for that month is made). The monthly payment must be a multiple of $10 and 
is the same for all months. Notice that it is possible for the balance to become negative using this payment scheme, which is okay.
"""
balance = 3926; 
annualInterestRate = 0.2
minimumFixedPayment = 0.0
myBalance = balance
monthlyInterestRate = annualInterestRate/12.0
while myBalance > 0.0:
    minimumFixedPayment += 10.0
    for month in range (0, 12):
        myBalance -= minimumFixedPayment
        myBalance += (myBalance * monthlyInterestRate)
    if myBalance > 0:
        myBalance = balance
print(minimumFixedPayment)
